import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './Bunny.scss'
import BunnyHypnoImage from './images/hypno-eyes.png'

export default class UserList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      animate: false,
      activeAttacker: ''
    }
  }

  componentWillReceiveProps (props) {
    const {activeAttacker} = props
    if (activeAttacker && activeAttacker !== this.props.activeAttacker) {
      setTimeout(() => {
        this.setState({animate: true, activeAttacker})
      }, 0)
    } else {
      this.setState({animate: false})
    }
  }

  render () {
    const {animate, activeAttacker} = this.state

    return (
      <div className={`bunny ${activeAttacker} ${animate ? `${activeAttacker}-animate` : ''}`}>
        <div className="char" />
        <div className={`bunny-eyes ${animate ? `bunny-eyes-animate` : ''}`}>
          <div className="hypnotized left">
            <img src={BunnyHypnoImage} alt="bunny-eyes" />
          </div>
          <div className="hypnotized right">
            <img src={BunnyHypnoImage} alt="bunny-eyes" />
          </div>
        </div>
      </div>
    )
  }
}

UserList.propTypes = {
  activeAttacker: PropTypes.string
}
