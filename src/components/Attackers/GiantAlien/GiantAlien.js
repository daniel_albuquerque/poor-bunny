import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './GiantAlien.scss'
import GiantAlienImage from './images/giant-alien.svg'
import GiantAlienHandFrontImage from './images/alien-hand-front.png'
import GiantAlienHandBackImage from './images/alien-hand-back.png'
import GiantAlienHypnoImage from './images/giant-alien-hypno.png'

export default class Ghost extends Component {
  constructor (props) {
    super(props)
    this.state = {animate: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (attacker) {
    this.props.onHover(attacker)
    this.setState({animate: attacker ? 'animate' : 'after-animate'})
  }

  render () {
    return (
      <div className="giant-alien-wrapper">
        <div
          onMouseEnter={this.onHover.bind(this, 'giant-alien')}
          onMouseLeave={this.onHover.bind(this, '')}
          className={`giant-alien ${this.state.animate}`}>
          <img src={GiantAlienImage} alt="giant-alien" />
        </div>
        <div className={`giant-alien-eyes ${this.state.animate}`}>
          <div className={`hypnotized left ${this.state.animate}`}>
            <img src={GiantAlienHypnoImage} alt="alien-eyes" />
          </div>
          <div className={`hypnotized right ${this.state.animate}`}>
            <img src={GiantAlienHypnoImage} alt="alien-eyes" />
          </div>
        </div>
        <div className={`giant-alien-hand ${this.state.animate}`}>
          <div className={`left ${this.state.animate}`}>
            <div className={`hand front ${this.state.animate}`}>
              <img src={GiantAlienHandFrontImage} alt="alien-hand" />
            </div>
            <div className={`hand back ${this.state.animate}`}>
              <img src={GiantAlienHandBackImage} alt="alien-hand" />
            </div>
          </div>
          <div className={`right ${this.state.animate}`}>
            <div className={`hand front ${this.state.animate}`}>
              <img src={GiantAlienHandFrontImage} alt="alien-hand" />
            </div>
            <div className={`hand back ${this.state.animate}`}>
              <img src={GiantAlienHandBackImage} alt="alien-hand" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Ghost.propTypes = {
  onHover: PropTypes.func
}
