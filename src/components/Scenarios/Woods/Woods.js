import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Bunny from 'Bunny'
import Ufo from 'Attackers/Ufo/Ufo'
import Ghost from 'Attackers/Ghost/Ghost'
import Alien from 'Attackers/Alien/Alien'

import './Woods.scss'

export default class Scenario extends Component {
  constructor (props) {
    super(props)
    this.state = {activeAttacker: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (activeAttacker) {
    this.setState({activeAttacker})
  }

  render () {
    return (
      <div className="scenario">
        <div className="background background-image">
          <Bunny activeAttacker={this.state.activeAttacker} />
          <Ufo onHover={this.onHover} />
          <Ghost onHover={this.onHover} />
          <Alien onHover={this.onHover} />
        </div>
      </div>
    )
  }
}

Scenario.propTypes = {
  attack: PropTypes.string
}
