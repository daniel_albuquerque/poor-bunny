import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Bunny from 'Bunny'
import Ghost from 'Attackers/Ghost/Ghost'

import './Horror.scss'

export default class Horror extends Component {
  constructor (props) {
    super(props)
    this.state = {activeAttacker: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (activeAttacker) {
    this.setState({activeAttacker})
  }

  render () {
    return (
      <div className="scenario">
        <div className="background background-image">
          <Bunny activeAttacker={this.state.activeAttacker} />
          <Ghost onHover={this.onHover} />
        </div>
      </div>
    )
  }
}

Horror.propTypes = {
  attack: PropTypes.string
}
