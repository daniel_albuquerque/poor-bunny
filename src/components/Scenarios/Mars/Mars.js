import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Bunny from 'Bunny'
import Ufo from 'Attackers/Ufo/Ufo'
import Alien from 'Attackers/Alien/Alien'
import GiantAlien from 'Attackers/GiantAlien/GiantAlien'

import './Mars.scss'
import BackgroundSpaceship from './images/background-spaceship.png'

export default class Scenario extends Component {
  constructor (props) {
    super(props)
    this.state = {activeAttacker: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (activeAttacker) {
    this.setState({activeAttacker})
  }

  render () {
    const {activeAttacker} = this.state

    return (
      <div className="mars-scenario">
        <div className="background background-image">
          <div className={`background-image-floor ${activeAttacker ? `${activeAttacker}-animate` : ''}`} />
          <div className="background-spaceship left"><img src={BackgroundSpaceship} alt="background-spaceship" /></div>
          <div className="background-spaceship right"><img src={BackgroundSpaceship} alt="background-spaceship" /></div>
          <Bunny activeAttacker={this.state.activeAttacker} />
          <Ufo onHover={this.onHover} />
          <GiantAlien onHover={this.onHover} />
          <Alien onHover={this.onHover} />
        </div>
      </div>
    )
  }
}

Scenario.propTypes = {
  attack: PropTypes.string
}
