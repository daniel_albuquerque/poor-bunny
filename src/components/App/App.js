import React, {Component} from 'react'
import {addLocaleData, IntlProvider} from 'react-intl'

/* import HorrorScenario from 'Scenarios/Horror/Horror'
import WoodsScenario from 'Scenarios/Woods/Woods' */
import MarsScenario from 'Scenarios/Mars/Mars'

/* Base styles */
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.scss'

export default class App extends Component {
  render () {
    return (
      <div className="app">
        <IntlProvider>
          <MarsScenario />
        </IntlProvider>
      </div>
    )
  }
}
