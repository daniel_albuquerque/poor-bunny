import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './Alien.scss'
import AlienImage from './images/alien.svg'
import TeleportationImage from './images/teleportation.svg'

const TELEPORTATION_SIZE = {height: 280}

export default class Ghost extends Component {
  constructor (props) {
    super(props)
    this.state = {animate: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (attacker) {
    this.props.onHover(attacker)
    this.setState({animate: attacker ? 'animate' : ''})
  }

  render () {
    return (
      <div className="alien-wrapper">
        <div
          onMouseEnter={this.onHover.bind(this, 'alien')}
          onMouseLeave={this.onHover.bind(this, '')}
          className="teleportation">
          <img height={TELEPORTATION_SIZE.height} src={TeleportationImage} alt="castle" />
        </div>
        <div className={`alien ${this.state.animate}`}>
          <img src={AlienImage} alt="alien" />
        </div>
      </div>
    )
  }
}

Ghost.propTypes = {
  onHover: PropTypes.func
}
