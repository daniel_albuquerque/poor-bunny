import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './Ufo.scss'
import UfoImage from './images/ufo.svg'

export default class Ufo extends Component {
  constructor (props) {
    super(props)
    this.state = {animate: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (attacker) {
    this.props.onHover(attacker)
    this.setState({animate: attacker ? 'animate' : ''})
  }

  render () {
    return (
      <div className="ufo">
        <div
          onMouseEnter={this.onHover.bind(this, 'ufo')}
          onMouseLeave={this.onHover.bind(this, '')}
          className="spaceship">
          <img src={UfoImage} alt="ufo" />
        </div>
        <div className={`abduction ${this.state.animate}`} />
      </div>
    )
  }
}

Ufo.propTypes = {
  onHover: PropTypes.func
}
