import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './Ghost.scss'
import GhostImage from './images/ghost.svg'
import CastleImage from './images/castle.svg'

export default class Ghost extends Component {
  constructor (props) {
    super(props)
    this.state = {animate: ''}
    this.onHover = this.onHover.bind(this)
  }

  onHover (attacker) {
    this.props.onHover(attacker)
    this.setState({animate: attacker ? 'animate' : ''})
  }

  render () {
    return (
      <div className="ghost">
        <div
          onMouseEnter={this.onHover.bind(this, 'ghost')}
          onMouseLeave={this.onHover.bind(this, '')}
          className="castle">
          <img src={CastleImage} alt="castle" />
        </div>
        <div className={`scare ${this.state.animate}`}>
          <img src={GhostImage} alt="ghost" />
        </div>
      </div>
    )
  }
}

Ghost.propTypes = {
  onHover: PropTypes.func
}
